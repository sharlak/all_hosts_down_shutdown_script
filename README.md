# README #

This script pings a list of hosts and if none of them respond
it powers off the machine. Best used with cron ;-)

User executing this script must be able to run shutdown command

USAGE: Edit the script and put a list of hosts in host_list and configure cron  to run this script periodically

```
#!shell
# Examples: 
# host_list=("192.168.1.1" "192.168.1.42")
# host_list=("www.youramazingwebpage.org anotherhost.com" "192.168.1.1")

```
Copyright 2014 Sharlak 
General Public License - GPL v3
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

See http://www.gnu.org/licenses/.